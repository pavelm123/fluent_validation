﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentValidation;
using FluentValidation.Results;

namespace ConsoleApplication1
{
    public class UnderTest
    {
        public int A { set; get; }
        public int B { set; get; }
        public object Ref { set; get; }

        public int Plus(int b)
        {
            B = b;
            return A + B;
        }
    }

    public class UnderTestValidator : AbstractValidator<UnderTest>
    {
        public UnderTestValidator()
        {
            //by default it is CascadeMode.Continue
            CascadeMode = CascadeMode.StopOnFirstFailure;

            //set globally:
            //ValidatorOptions.CascadeMode = CascadeMode.StopOnFirstFailure;

            RuleFor(x => x.A).GreaterThanOrEqualTo(0).WithMessage("A must be greater than zero");
            RuleFor(x => x.Ref).NotNull().WithMessage("Ref must be not null");
            RuleFor(x => x.B).NotEqual(x=>x.A).WithMessage("B should not equal A");
            RuleFor(x => x.B).NotEqual(x => x.A* x.A).WithMessage("B should not equal A*A");
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            var u = new UnderTest() {A = -3, B = 9};
            UnderTestValidator utv = new UnderTestValidator();
            var res = utv.Validate(u);
            bool isOK = res.IsValid;
            IList<ValidationFailure> failures = res.Errors;
            if (!isOK)
                foreach (var failure in failures)
                {
                    Console.WriteLine($"{failure.PropertyName}:{failure.AttemptedValue}:{failure.ErrorMessage}");
                }
        }
    }
}
